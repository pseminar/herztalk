{ heartTalkPresentation ? { outPath = ./.; name = "DIR"; }
, pkgs ? import <nixpkgs> {}
}:

let
  stdenv = pkgs.stdenv;
  buildBowerComponents = pkgs.buildBowerComponents;
in stdenv.mkDerivation {
  name = "bower-revealjs-heartTalk";
  src = heartTalkPresentation;

  bowerComponents = buildBowerComponents {
    name = "bower-revealjs-bowercmps-heartTalk";
    generated = ./bower-generated.nix;
    src = heartTalkPresentation;
  };

  buildPhase = ''
    cp --reflink=auto --no-preserve=mode -R $bowerComponents/bower_components .
    export HOME=$PWD
  '';

  installPhase = "mv bower_components $out";
}
